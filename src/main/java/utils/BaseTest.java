package utils;


import mocks.MockConfig;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import static io.restassured.RestAssured.*;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

public class BaseTest {

    @BeforeTest
    protected void precondition() {
      config = newConfig().encoderConfig(encoderConfig().defaultContentCharset("UTF-8"));
      baseURI = "http://localhost:8090";
      basePath = "/reqres.in/";
      MockConfig.startMock();
   }

   @AfterTest
    protected void postCondition(){
        MockConfig.stopMock();
   }
}
