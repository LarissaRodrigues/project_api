package tasks;

import io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.*;

public class Task {

    private static ValidatableResponse validatableResponse;
    private static Response response;

    public ValidatableResponse getResponse(String api){

        validatableResponse = given().log().all().
                when().
                get(api).then();

        return validatableResponse;
    }

    private static String requestBody = "{\n" +
            "  \"title\": \"foo\",\n" +
            "  \"body\": \"bar\",\n" +
            "  \"userId\": \"1\" \n}";

    public ValidatableResponse postResponse(String api){
        return validatableResponse = given().log().all()
                .body(requestBody)
                .post(api)
                .then();
    }

    public Response postRequest(String api) {
        return response = given().headers("Content-type", "application/json")
                .and()
                .body(requestBody).when().post(api).then().extract().response();
    }
}
