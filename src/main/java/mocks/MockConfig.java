package mocks;

import com.github.tomakehurst.wiremock.WireMockServer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class MockConfig {

    private MockConfig() throws UnsupportedOperationException{
        throw new UnsupportedOperationException("Está é uma classe utilitária e não pode ser instanciada!");
    }

    private static WireMockServer wireMockServer;
    private static final Logger LOGGER = LogManager.getLogger();
    private static final int PORT = 8090;

    public static void startMock() {
        wireMockServer = new WireMockServer(PORT);
        wireMockServer.start();
        LOGGER.info("Inicializando Mock");
    }

    public static void stopMock() {
        wireMockServer.stop();
        LOGGER.info("Finalizando Mock");
    }

    public static void setupStub() {
        wireMockServer.stubFor(get(urlEqualTo("/reqres.in")));
               // .willReturn(aResponse().withBodyFile("/apis/")));
                //.willReturn(aResponse().withStatus(200)));
    }

    public static void statusMessage() {
        wireMockServer.stubFor(get(urlEqualTo("/reqres.in/some/thing"))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withStatusMessage("Everything was just fine!")
                        .withHeader("Content-Type", "text/plain")));
    }

    public static void statusMenssagerPost(){
        wireMockServer.stubFor(post(urlEqualTo("/reqres.in/two"))
                        .willReturn(ok("Body content").withStatus(201)
                                .withHeader("Content-Type", "application/json")
                                .withBody("Literal text to put in the body"))
                );
    }

    public static void statusPost(){
        wireMockServer.stubFor(post(urlEqualTo("/reqres.in/two/one"))
                .willReturn(ok("Body content").withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody("Literal text to put in the body"))
        );
    }
}
