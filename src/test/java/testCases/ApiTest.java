package testCases;

import com.github.tomakehurst.wiremock.WireMockServer;
import mocks.MockConfig;
import org.testng.annotations.Test;
import tasks.Task;
import utils.BaseTest;

import static org.hamcrest.Matchers.is;
import static org.testng.AssertJUnit.assertEquals;

public class ApiTest extends BaseTest {

    private Task task = new Task();
    private static WireMockServer wireMockServer;

    @Test
    public void runTest(){
        task.getResponse("/api/users").log().all();
    }

    @Test
    public void runTest404(){
        task.getResponse("/api/users/customers").log().all();
    }

    @Test
    //Duas formas de simular o retorno ou inserindo elas completamente no arquivo mock.json dentro da pasta mappings
    // Ou colocando elas dentro do código java como dentro no método statusMensager
    public void statusMessage() {
           MockConfig.statusMessage();
           task.getResponse("/some/thing").statusCode(is(500)).log().all();
    }

    @Test
    public void runTestPost(){
        MockConfig.statusMenssagerPost();
        task.postResponse("/two").log().all();
    }

    @Test
    public void runTestPost2(){
        MockConfig.statusPost();
        task.postRequest("/two/one");
        assertEquals(201,  task.postRequest("/two/one").statusCode());
    }
}
